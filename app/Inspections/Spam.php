<?php

namespace App\Inspections;

class Spam
{
    protected $Inspections = [
        InvalidKeywords::class,
        KeyHeldDown::class,
    ];

    public function detect($body)
    {
        foreach ($this->Inspections as $inspection) {
            (new $inspection)->detect($body);
        }

        return false;
    }
}
